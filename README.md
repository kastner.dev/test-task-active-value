# csv-importer

CSV importer test task based on Symfony 5.

Author: Cedric Kastner <cedric@kastner.wtf>

Copyright (c) 2020 active value GmbH.  
All rights reserved.

## Requirements 

- PHP >= 7.3.14
- MySQL/MariaDB >= 8.0.19
- Composer >= 1.9.3
- Symfony >= 5.1.5
- Symfony CLI >= 4.18.4

## Configuration

1. Configure `.dotenv` to match your database configuration

## Installation

1. Make sure you did run the Configuration step once before
2. Use `vagrant up` to start the vagrant box (if not already started)
3. Change directory to `/var/www` by issueing `cd /var/www`
4. Run `bin/console doctrine:database:create` once to create the database scheme
5. Run `bin/console doctrine:migrations:migrate` once to create the database table

## How to run the importer

1. Use `vagrant up` to start the vagrant box (if not already done)
2. Change directory to `/var/www` by issuing `cd /var/www`
3. Run `bin/console app:csv-import test.csv` to import the demo CSV file into the database

_Notice:_ You can also update record properties except article ID, in order to update existing products already imported into the database.

## How to run the importer on a daily basis

1. Edit the crontab using `sudo crontab -e` and enter the following line for executing this task every day at 3 A.M. UTC:

```
0 3 * * * cd /var/www/ && bin/console app:csv-import test.csv >/dev/null 2>&1
```
