<?php
namespace App\Command;

use \Iterator;
use League\Csv\Reader;
use League\Csv\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use App\Entity\Product;
use App\Repository\ProductRepository;

/**
 * CsvImportCommand
 */
class CsvImportCommand extends Command
{
    /** @var string Name of this command */
    protected static $defaultName = 'app:csv-import';

    /** @var SymfonyStyle InputOutput interface */
    private $io;

    /** @var ContainerInterface */
    private $container;

    /** @var EntityManagerInterface */
    private $em;

    /** @var ProductRepository */
    private $repo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Imports products from a CSV file into the database')
            ->addArgument('filename', InputArgument::REQUIRED, 'Filename of CSV file to import products from')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Command-wide injections
        $this->io = new SymfonyStyle($input, $output);
        $this->em = $this->container->get('doctrine')->getManager();
        $this->repo = $this->em->getRepository(Product::class);

        $filename = $input->getArgument('filename');

        // Check if we have a kind-of "valid" filename
        if (!$filename || empty(trim($filename)) || !file_exists($filename)) {
            $this->io->error('Empty/missing filename or file not found');
            return Command::FAILURE;
        }

        // Import CSV from file
        if ($csv = $this->importCSVFromFile($filename)) {
            foreach($csv as $record) {
                $this->insertRecordIntoTable($record);
            }

            $this->io->success('CSV import finished successfully');
            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }

    /**
     * Import CSV data from file
     *
     * @param string $filename Filename to import CSV data from
     */
    private function importCSVFromFile(string $filename): ?Iterator {
        try {
            $csv = Reader::createFromPath($filename)
                ->setDelimiter(";")
                ->setEnclosure('"')
                ->setEscape('\\');
        } catch (Exception $e) {
            $this->io->error('Error reading CSV data from file');
        }

        if (is_object($csv)) {
            return $csv->getRecords();
        }

        return null;
    }

    /**
     * Inserts a CSV record into the DB table
     *
     * @param array $record Array with record data
     * @return bool
     */
    private function insertRecordIntoTable(array $record): bool {
        if ($product = $this->repo->findOneByArticleNumber($record[0])) {
            // Update existing product
            $product->setTitle(trim($record[1]))
                ->setTaxPercent(intval($record[2]))
                ->setPrice(floatval($record[3]))
                ->setLongDescription(trim($record[4]))
                ->setUpdatedAt(new \DateTime());
        } else {
            // Insert new product
            $product = new Product();
            $product->setArticleNumber(trim($record[0]))
                ->setTitle(trim($record[1]))
                ->setTaxPercent(intval($record[2]))
                ->setPrice(floatval($record[3]))
                ->setLongDescription(trim($record[4]))
                ->setInsertedAt(new \DateTime());
        }

        try {
            // Persist to database
            $this->em->persist($product);
            $this->em->flush();
        } catch (ORMException $e) {
            $this->io->error('Error saving CSV record to database');
            return false;
        }

        return true;
    }

    /**
     * Deletes a CSV record from the DB table
     *
     * @param array $record
     * @return bool
     */
    private function deleteRecordFromTable(array $record): bool {
        if ($productToDelete = $this->repo->findOneByArticleNumber($record[0])) {
            try {
                $this->em->remove($productToDelete);
                $this->em->flush();
            } catch (ORMException $e) {
                $this->io->error('Error deleting record from database');
            }
        }
    }
}
